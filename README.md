# Hip Conditions CTA
Everything needed to manage conditions for Hip client sites.

## Installation 
Install by running the following command from the root of your WordPress project:  
`composer require hipdevteam/conditions`
